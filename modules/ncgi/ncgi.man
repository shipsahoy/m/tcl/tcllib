[vset VERSION 1.4.3]
[manpage_begin ncgi n [vset VERSION]]
[see_also html]
[keywords CGI]
[keywords cookie]
[keywords form]
[keywords html]
[comment {-*- tcl -*- doctools manpage}]
[copyright {2018  Poor Yorick}]
[moddesc   {CGI Support}]
[titledesc {Procedures to manipulate CGI values.}]
[category  {CGI programming}]
[require Tcl 8.4]
[require ncgi [opt [vset VERSION]]]
[description]
[para]

[package ncgi] provides routines to manipulate [
    uri https://tools.ietf.org/html/rfc3875 CGI
]
request arguments provided in the query string and form data, to set and get
cookies, and to encode and decode www-url-encoded values.

[para]

[cmd ::ncgi] is a routine whose subroutines are the same as those documented
below for the [const ::ncgi] namespace.  When calling these routines as
subroutines, omit [arg session] as it is passed automatically. 

To create a new CGI session, call [cmd {::ncgi .new}], and then use command it
creates returns to work with request arguments.  Use [cmd {::ncgi get}] to
retrieve argument values, or use [cmd {::ncgi all}] for arguments that might
occur multiple times.

If only one CGI session is needed in the interpreter, [const ::ncgi] may be used
directly.

[para]



[section Definitions]

[list_begin definitions]

[def multidict]

A dictionary where the keys may not be unique

[def {argument multidict}]

A multidict where keys are names of request arguments provided in the
query string and form data, and each value is itself a list containing the
value for the argument and the corresponding dictionary of parameters.

[list_end]

[section Procedures]

[list_begin definitions]

[call [cmd ::ncgi] [method .new] [arg name] [opt "[arg option] [arg value]"]]

Creates a routine named [arg name] that represents a new CGI session, and
returns the name the new routine.  The available options, primarily for use
in the test suite, are

[list_begin options]
[opt_def body [arg body]]

Use [arg body] as the body for the purpose of processing form data.

[opt_def contenttype [arg contenttype]]

Use [arg contenttype] as the contenttype for the purpose of processing form data.

[opt_def form [arg formdata]]

Use [arg formdata] as the form data.

[opt_def querystring [arg querystring]]

Use [arg querystring] as the query string.


[list_end]


[call [arg session] [method all] [arg name] [opt [arg default]]]

Like [cmd ::ncgi::get], but returns a list of values matching [arg name] in the
argument multidict.


[call [arg session] [method body]]

Returns the raw request body.


[call [arg session] [method {cookies all}]]

Returns a multidict of all cookie names and values.


[call [arg session] [method {cookies get}] [opt [arg name]] [opt [arg default]]]

Returns the value of the last cookiet named [arg name], or [arg default] it is
provided and no such cookie exists.


[call [arg session] [method decode] [arg str]]

Decodes the www-url-encoded [arg str]. In this encoding special characters are
represented with a %xx sequence, where xx is the character code in hex.


[call [arg session] [method encode] [arg string]]

Encodes [arg string] into www-url-encoded format.


[call [arg session] [method exists] [arg name]]

Returns [const true] if an CGI argument matching [arg name] is present and is
[const false] otherwise.


[call [arg session] [method form] [cmd get] [opt [arg name]]]

Like [cmd ::ncgi::get], but only considers form arguments, not arguments
in the query string.


[call [arg session] [method get] [opt [arg name]] [opt [arg default]]]

Returns a list containing the values matching [arg name] in the argument
multidict.  If [arg name] is not provided, returns the entire argument
multidict.


[call [arg session] [method {header send}] [opt [arg type]]]

Serialize the response header to the session output.  Produces a [
    const Content-Type
]
header and additional headers based on [arg args], which is a multidict of
names and values. [arg type] defaults to [const text/html].


[call [arg session] [cmd importFile] [arg cmd] [arg name] [
    opt [arg filename]]]

Provides information about an uploaded file from a form field.

Possible values for [arg cmd] are

[list_begin definitions]

[def "[option -client] [arg name]"]

Returns the filename as sent by the client.

[def "[option -type] [arg name]"]

Returns the mime type of the uploaded file.

[def "[option -data] [arg name]"]

Returns the contents of the file.

[def "[option -server] [arg name]"]

Returns the name of a channel routine for the contents of the field named
[arg name].

[list_end]


[call [arg session] [method {query get}]]

Returns the query data as a multidict.


[call [arg session] [method {query set}] [arg {name value}]]

Sets a query value.


[call [arg session] [method {query string}]]

Returns the raw query data.


[call [arg session] [method redirect] [arg url]]

Generates a response that causes a 302 redirect by the Web server.  The
[arg url] is the new URL that is the target of the redirect.  The URL
will be qualified with the current server and current directory, if
necessary, to convert it into a full URL.


[call [arg session] [method type]]

Returns the Content-Type of the current CGI values.


[call [cmd ::ncgi::urlStub] [opt [arg url]]]

Returns the current URL, but without the protocol, server, and port.
If [arg url] is specified, then it defines the URL for the current
session.  That value will be returned by future calls to

[cmd ::ncgi::urlStub]


[list_end]

[section Examples]

Uploading a file
[example {
HTML:
<html>
<form action="/cgi-bin/upload.cgi" method="POST" enctype="multipart/form-data">
Path: <input type="file" name="filedata"><br>
Name: <input type="text" name="filedesc"><br>
<input type="submit">
</form>
</html>

TCL: upload.cgi
#!/usr/local/bin/tclsh

::ncgi::parse
set filedata [::ncgi::value filedata]
set filedesc [::ncgi::value filedesc]

puts "<html> File uploaded at <a href=\"/images/$filedesc\">$filedesc</a> </html>"

set filename /www/images/$filedesc

set fh [open $filename w]
puts -nonewline $fh $filedata
close $fh
}]

[para]

[vset CATEGORY ncgi]
[include ../doctools2base/include/feedback.inc]
[manpage_end]
